<a name="unreleased"></a>
## [Unreleased]

### Bug Fixes
- changelog generation


<a name="v0.0.0"></a>
## v0.0.0 - 2021-10-29
### Documentation
- lint docs readme markdown
- update documentation read me ([#6](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/6))
- update readme with quickstart links ([#5](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/5))
- update readme with quickstart links ([#5](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/5))
- update readme with quickstart links ([#5](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/5))
- update contributing guidelines ([#2](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/2))
- update contributing guidelines ([#2](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/2))
- update contributing guidelines ([#2](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/2))
- add contributing guidelines ([#2](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/2))
- update dod with attributions ([#7](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/7))
- update dod with attributions ([#7](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/7))
- add code of conduct ([#1](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/issues/1))

### Pull Requests
- Merge branch 'docs/docs-readme' into 'main'
- Merge branch 'docs/readme-quickstart' into 'main'
- Merge branch 'docs/readme-quickstart' into 'main'
- Merge branch 'docs/readme-quickstart' into 'main'
- Merge branch 'docs/contributing-guidelines' into 'main'
- Merge branch 'docs/contributing-guidelines' into 'main'
- Merge branch 'docs/contributing-guidelines' into 'main'
- Merge branch 'doc/update-dod-attribution' into 'main'
- Merge branch 'doc/update-dod-attribution' into 'main'
- Merge branch 'doc/code-of-conduct' into 'main'


[Unreleased]: https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/compare/v0.0.0...main
