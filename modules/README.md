<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/gitlab-logo-gray-rgb.png" width="400" />
</a>

[![Maintained by CPrime Elite Engineering](https://img.shields.io/badge/maintained%20by-cprime%20elite%20engineering-FC6D26)](https://cprime.com/)
[![Built for Engineers](https://img.shields.io/badge/project-monorepo%20devops%20template-FC6D26)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template)
[![Latest](https://img.shields.io/badge/latest-0.0.0-FC6D26)](../../releases)
[![Chat on Gitter](https://img.shields.io/badge/community%20&%20support-chat%20on%20gitter-FC6D26)](https://gitter.im/cprime-elite-engineering/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
# Modules Directory

In this directory, we maintain the individual 'repo modules' that constitute this monorepo.
# Build Status

[![pipeline status](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/badges/main/pipeline.svg)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/-/pipelines)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

# Overview

A 'repo module' should not be confused with a 'Git submodule.' Submodules allow you to keep a Git repository as a subdirectory of another Git repository. With the monorepo design pattern, we explicitly do not divide our project into multiple Git repositories and maintain all code under a single repository.

Google, Facebook, Twitter, Uber, and Microsoft advocate the monorepo design pattern for version control. Microsoft host the world's largest monorepo for the Microsoft Windows Operating System. Each of these companies demonstrates elite performance in their software development and operations. Appropriate use of monorepos is a contributing factor to that elite performance.

Describing the benefits and trade-offs for a monorepo vs. polyrepo approach is too complex a subject to include in this readme. For those that are interested, these external links will provide additional information:

https://kinsta.com/blog/monorepo-vs-multi-repo/

https://www.atlassian.com/git/tutorials/monorepos

https://en.wikipedia.org/wiki/Monorepo

## License

We release all code within this repo under the MIT License. Please see [LICENSE](/LICENSE) and [NOTICE](/NOTICE) for more details.

<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/dld-cp-sponsor.png" />
</a>
