<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/gitlab-logo-gray-rgb.png" width="400" />
</a>

[![Maintained by CPrime Elite Engineering](https://img.shields.io/badge/maintained%20by-cprime%20elite%20engineering-FC6D26)](https://cprime.com/)
[![Built for Engineers](https://img.shields.io/badge/project-monorepo%20devops%20template-FC6D26)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template)
[![Latest](https://img.shields.io/badge/latest-0.0.0-FC6D26)](../../releases)
[![Chat on Gitter](https://img.shields.io/badge/community%20&%20support-chat%20on%20gitter-FC6D26)](https://gitter.im/cprime-elite-engineering/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

# Fibonacci Service (Spring Boot)

A spring boot microservice implementation for the Monrepo DevOps template.

# Build Status

[![pipeline status](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/badges/main/pipeline.svg)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/-/pipelines)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

## Sonarcloud Analysis

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=alert_status)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=security_rating)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=bugs)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=ncloc)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=coverage)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=code_smells)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=gitlab-dojo_monorepo-devops-template-fibonacci-service&metric=sqale_index)](https://sonarcloud.io/dashboard?id=gitlab-dojo_monorepo-devops-template-fibonacci-service)

## Documentation

Comprehensive documentation for this repo can be found in the [gitlab pages](https://gitlab-dojo.gitlab.io/gitlab-core-oss/monorepo-devops-template/) website hosted within this repo. Review the [docs](/docs) directory within this repo to familiarize yourself with how our documentation is maintained and deployed from CICD.

## Project Management

Tasks and issues that relate to development of this repo can be found in this repo's [project board](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/-/boards/3445113).

## How to use this repo.

Follow these steps to fully implement this repo:

### Step 0: GitLab.

If you are viewing this readme by any other means go to the canonical repository on [GitLab](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template) for the most up to date version of this repo.
### Step 1: Launch Gitpod IDE.

- In https://gitlab.com/-/profile/preferences ensure you have the Integrations - Enable Gitpod integration option checked.
- On the home page for this repo, there is a dropdown option to the left of the download button. It will read 'Web IDE' or 'Gitpod.' Select 'Gitpod.'
- Then click on the selected 'Gitpod' option to launch the Gitpod Browser IDE (you can also right-click to open in a new tab). Alternatively click this url https://gitpod.io/#gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template
- Gitpod will now launch.
- Upon launch, GitPod will execute static code analysis using [pre-commit](https://pre-commit.com/) to verify the branch you are working on conforms to our coding guidelines. Allow this process to complete. A successful analysis of the code base will look something like this:

``` plaintext
Check Yaml...............................................................Passed
Fix End of Files.........................................................Passed
Trim Trailing Whitespace.................................................Passed
Check for case conflicts.................................................Passed
Check for merge conflicts................................................Passed
yamllint.................................................................Passed
Dockerfile linter........................................................Passed
Google Java Formatter....................................................Passed
pmd......................................................................Passed
cpd......................................................................Passed
```

### Step 2: Run the tests.

- Within the root directory of this project, execute developer tests for this repo module from the command line:

``` bash
make fibonacci-service/test-local
```

- Other command line options are:

``` bash
fibonacci-service/package-local
fibonacci-service/containerize-local
fibonacci-service/run-local
```

### Step 3: Eat, Sleep, Code, Repeat!

- Create a local working branch for whatever feature you are working on.
- Run local tests to ensure you won't push a breaking build.
- When committing your code within GitPod, ensure all [pre-commit](https://pre-commit.com/) static code analysis tests pass.
- Push your branch to origin to trigger an automated CI build.
- If the automated CI build passed, create a pull request to have your code merged to the master branch.
- Rinse and repeat! Happy coding :)

## Who maintains this repo?

Cprime Elite Engineering maintains this repo. If you're looking for support, send an email to [elite-engineering@cprime.com](mailto:elite-engineering@cprime.com?subject=Monorepo%20DevOps%20Template).
CPrime Elite Engineering can help with:

- Setup, customization, and support for GitLab.
- Modules for Infrastructure as Code, such as VPCs, Docker Clusters, and Databases.
- Establishing production-grade CICD pipelines, including test automation, security validation, and continuous delivery.
- Automated quality assurance for compliance requirements, such as HIPAA.
- Coaching & Training for DevOps.

## How do I contribute to this repo?

Contributions are welcome. Check out the
[Contribution Guidelines](https://gitlab-dojo.gitlab.io//gitlab-core-oss/monorepo-devops-template/contributiion-guidelines.html) and [Code of Conduct](https://gitlab-dojo.gitlab.io//gitlab-core-oss/monorepo-devops-template/code-of-conduct.html) for instructions.

## Tooling

The following tools are not strictly required to work with this repo but we recommend their use and used them oursleves in the creation of this repo:

- [GitLab](https://Gitlab.com): GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration and deployment pipeline features, using an open-source license, developed by GitLab Inc.
- [Gitpod](https://www.gitpod.io/): Gitpod streamlines developer workflows by providing prebuilt, collaborative development environments in your browser - powered by VS Code.

## How is this repo versioned?

This repo follows the principles of [Semantic Versioning](http://semver.org/). You can find each new release,
along with the changelog, in the [releases page](../../releases) of this repo.

During initial development, the major version will be 0 (e.g., `0.x.y`), which indicates the code does not yet have a
stable API. Once we hit `1.0.0`, we will make every effort to maintain a backward-compatible API and use the MAJOR,
MINOR and PATCH versions on each release to indicate any incompatibilities.

Publication of the CHANGELOG for this repo is automated using [git-changelog](https://github.com/git-chglog/git-chglog) in the style of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Architectural Decision Records

We capture all architectural decisions made in the authoring of this repo as a log of [Architecture Decision Records](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions) (ADRs). You can view the ADR log in the GitLab pages documentation site hosted within this repo [here](https://gitlab-dojo.gitlab.io//gitlab-core-oss/monorepo-devops-template/architectural-decision-record.html).

We automate the creation of architectural decision records relating to this repo using [adr-tools](https://github.com/npryce/adr-tools).


## License

We release all code within this repo under the MIT License. Please see [LICENSE](/LICENSE) and [NOTICE](/NOTICE) for more details.

<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/dld-cp-sponsor.png" />
</a>
