<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/gitlab-logo-gray-rgb.png" width="400" />
</a>

[![Maintained by CPrime Elite Engineering](https://img.shields.io/badge/maintained%20by-cprime%20elite%20engineering-FC6D26)](https://cprime.com/)
[![Built for Engineers](https://img.shields.io/badge/project-monorepo%20devops%20template-FC6D26)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template)
[![Latest](https://img.shields.io/badge/latest-0.0.0-FC6D26)](../../releases)
[![Chat on Gitter](https://img.shields.io/badge/community%20&%20support-chat%20on%20gitter-FC6D26)](https://gitter.im/cprime-elite-engineering/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

# Contribution Guidelines

You can read our comprehensive contribution guidelines [here](https://gitlab-dojo.gitlab.io//gitlab-core-oss/monorepo-devops-template/contributiion-guidelines.html) in our GitLab pages documentation site.

## License

We release all code within this repo under the MIT License. Please see [LICENSE](/LICENSE) and [NOTICE](/NOTICE) for more details.

<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/dld-cp-sponsor.png" />
</a>
