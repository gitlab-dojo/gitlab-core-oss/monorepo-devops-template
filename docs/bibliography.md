---
layout: default
title: Bibliography
nav_order: 6
has_children: false
---

# Bibliography

## Scrumban: Essays on Kanban Systems for Lean Software Development

Corey Ladas
Publisher: Modus Cooperandi Press (March 19, 2011)
Publication Date: March 19, 2011
https://www.amazon.com/Scrumban-Essays-Systems-Software-Development-ebook/dp/B004SY63BY

## The Scrum Guide, The definitive guide to Scrum : The rules of the Game

Ken Schwaber and Jeff Sutherland
©2017 Ken Schwaber and Jeff Sutherland
https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-US.pdf

## Kanban: Successful Evolutionary Change for Your Technology Business

David J. Anderson
Publisher: Blue Hole Press; 3.8.2010 edition (April 7, 2010)
https://www.amazon.com/Kanban-Successful-Evolutionary-Technology-Business/dp/0984521402/

## Testing with Humans: How to use experiments to drive faster, more informed decision making

Giff Constable
Publication Date: October 16, 2018
Copyright ©2018 Giff Constable
https://www.amazon.com/Testing-Humans-experiments-informed-decision-ebook/dp/B07JGQ2GYR

## Extreme Programming Explained: Embrace Change, 2nd Edition (The XP Series)

Kent Beck
Publisher: Addison-Wesley; 2nd edition (November 26, 2004)
https://www.amazon.com/Extreme-Programming-Explained-Embrace-Change/dp/0321278658/
