<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/gitlab-logo-gray-rgb.png" width="400" />
</a>

[![Maintained by CPrime Elite Engineering](https://img.shields.io/badge/maintained%20by-cprime%20elite%20engineering-FC6D26)](https://cprime.com/)
[![Built for Engineers](https://img.shields.io/badge/project-monorepo%20devops%20template-FC6D26)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template)
[![Latest](https://img.shields.io/badge/latest-0.0.0-FC6D26)](../../releases)
[![Chat on Gitter](https://img.shields.io/badge/community%20&%20support-chat%20on%20gitter-FC6D26)](https://gitter.im/cprime-elite-engineering/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

# READ THE DOCS!

The documentation for this repo is built and deployed by a CICD pipeline and then deployed to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) on this URL:

https://gitlab-dojo.gitlab.io/gitlab-core-oss/monorepo-devops-template/

# Build Status

[![pipeline status](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/badges/main/pipeline.svg)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/-/pipelines)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

# Overview

As part of our [Definition of Done](https://gitlab-dojo.gitlab.io//gitlab-core-oss/monorepo-devops-template/definition-of-done.html#dod-merge-request) for a merge request, documentation must be complete for any given feature.

We consider documentation expressed as markdown to be code. Therefore, it is branched, merged, and analyzed for quality like any other code. It is subject to peer review before merging into a main branch of the codebase. Inclusion in the codebase means that we can manage this process using the same tools for any other coding language.

# How to update the documentation.

Follow these steps to update the documentation:

## Step 0: GitLab.

If you are viewing this readme by any other means go to the canonical repository on GitLab for the most up to date version of this repo.
### Step 1: Launch Gitpod IDE.

- In https://gitlab.com/-/profile/preferences ensure you have the Integrations - Enable Gitpod integration option checked.
- On the home page for this repo, there is a dropdown option to the left of the download button. It will read 'Web IDE' or 'Gitpod.' Select 'Gitpod.'
- Then click on the selected 'Gitpod' option to launch the Gitpod Browser IDE (you can also right-click to open in a new tab). Alternatively click this url https://gitpod.io/#gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template
- Gitpod will now launch.
- Upon launch, GitPod will execute static code analysis using [pre-commit](https://pre-commit.com/) to verify the branch you are working on conforms to our coding guidelines. Allow this process to complete. A successful analysis of the code base will look something like this:

``` bash
Check Yaml...............................................................Passed
Fix End of Files.........................................................Passed
Trim Trailing Whitespace.................................................Passed
Check for case conflicts.................................................Passed
Check for merge conflicts................................................Passed
yamllint.................................................................Passed
Dockerfile linter........................................................Passed
Google Java Formatter....................................................Passed
pmd......................................................................Passed
cpd......................................................................Passed
```

### Step 2: Preview the documentation site in GitLab.
- To generate the documentation for this repo, we use [Jekyll](https://jekyllrb.com/). Jekyll is a static site generator written in Ruby by Tom Preston-Werner, co-founder of GitHub for personal, project, or organization websites.
- Open the '_config.yml' file in the /docs directory and comment out these two lines:

``` yml
url: https://gitlab-dojo.gitlab.io/
baseurl: gitlab-core-oss/monorepo-devops-template
```

- Replace the `url:` declaration with the unique IP address of your GitPod instance and prepend `https://4000-` to that IP address. Example below:

``` yml
# url: https://gitlab-dojo.gitlab.io/
# baseurl: gitlab-core-oss/monorepo-devops-template
url: https://4000-https://coral-gerbil-z5nlbe52.ws-eu18.gitpod.io/
```

- From within the root directory of this repo, execute the following command:

``` bash
make jekyll/serve
```

- A dialogue window will appear and inform you a service is available on port 4000. Click make public.
- In the remote explorer tab of the GitPod IDE, click open browser for port 4000.
- A preview of the documentation site is now available.

### Step 3: Edit Markdown
- Navigate to the `/docs` directory of this repo in the GitPod IDE.
- Add new markdown files or make your updates to existing files.
- While the Jekyll server is running in your IDE, any changes you make to these files will regenerate HTML on the fly within the site preview.
- Our documentation site uses the [just-the-docs](https://github.com/pmarsceill/just-the-docs) documentation theme. You can read full developer documentation for this theme [here](https://pmarsceill.github.io/just-the-docs/).

# Architectural Decision Records

We capture all architectural decisions made in the authoring of this repo as a log of [Architecture Decision Records](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions) (ADRs). You can view the ADR log in the GitLab pages documentation site hosted within this repo [here](https://gitlab-dojo.gitlab.io//gitlab-core-oss/monorepo-devops-template/architectural-decision-record.html).

We automate the creation of architectural decision records relating to this repo using [adr-tools](https://github.com/npryce/adr-tools). We have configured adr-tools to publish the decision record so that it generates HTML for the documentation site.

To create a new ADR, do the following.

- From any directory in this repo at the command line execute the following command:

``` bash
adr new <the title of your architectural decision record>
```
- A new ADR file will be generated and will open in the GitPod IDE editor. Make your changes as with any other markdown file and create a merge request for review.
## License

We release all code within this repo under the MIT License. Please see [LICENSE](/LICENSE) and [NOTICE](/NOTICE) for more details.

<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/dld-cp-sponsor.png" />
</a>
